#!/bin/bash
# Purging *systemd*
apt -y purge *systemd*
update-grub

# Desktops
echo 'Enabled desktops envirometn to install:
0 - None
1 - JWM
2 - LXDE
'
read -p "Select the number of the desktop to enviroment you wish to install, or 0 to not install any: " desktop
case $desktop in
	0 )
		echo 'none selected desktop'
	;;
	
# JWM Minimal (Mini Loc-OS)
1 )
	apt update
	apt install -y acpi adwaita-icon-theme alsa-utils connman-gtk galculator gir1.2-gtk-3.0 gnome-icon-theme gnome-screenshot htop jwm leafpad lxappearance pavucontrol pcmanfm playerctl pulseaudio python3-gi rofi sakura xorg
	touch /opt/Loc-OS-LPKG/installed-lpkg/LinksLpkgRepo.list
	wget https://gitlab.com/loc-os_linux/debian12-to-loc-os23/-/raw/main/jwm-config.tar.xz
	tar xvf jwm-config.tar.xz
	rm -f jwm-config.tar.xz

	cp -rf jwm-config/* /
	cp -rf jwm-config/etc/skel/.config /home/*/
	cp -rf jwm-config/etc/skel/.bashrc /home/*/
	cp -rf jwm-config/etc/skel/.face* /home/*/
	rm -rf jwm-config
	rm -f debian12-to-loc-os23.sh
	;;
# LXDE
2 )
		touch /etc/apt/preferences.d/lightdm
		chmod 777 /etc/apt/preferences.d/lightdm
echo "Package: *lightdm*:any
Pin: origin *
Pin-Priority: -1" > /etc/apt/preferences.d/lightdm
	apt update
	apt install -y acpi adwaita-icon-theme connman-gtk galculator gnome-themes-extra gparted leafpad lpkg lxappearance lxappearance-obconf lxhotkey-plugin-openbox lxinput lxde-core lxdm lxdm-loc-os mintstick obsidian-icon-theme picom plank playerctl rofi synaptic xorg yad
	touch /opt/Loc-OS-LPKG/installed-lpkg/LinksLpkgRepo.list
	wget https://gitlab.com/loc-os_linux/debian12-to-loc-os23/-/raw/main/lxde-config.tar.xz
	tar xvf lxde-config.tar.xz
	rm -f lxde-config.tar.xz
	cp -r lxde-config/usr/* /usr
	cp -r lxde-config/skel/.* /etc/skel/
	cp -r lxde-config/opt/Loc-OScc /opt
	cp -r lxde-config/opt/.CcLoc-OS /opt
	rm -rf lxde-config
	cp -rf /etc/skel/.* /home/*/
	chmod -R 777 /home/*/.*
	mv /usr/libexec/xscreensaver/xscreensaver-systemd /usr/libexec/xscreensaver/xscreensaver-sysvinit
	rm -f debian12-to-loc-os23.sh
	;;
	
# Invalid
* )
		echo 'Invalid option, continuing without desktop'
	;;
esac

echo 'Finished, you can reboot now.'
rm -f desktops.sh

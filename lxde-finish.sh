#!/bin/bash
# Purging *systemd*
apt -y purge *systemd*
update-grub

# Dekstops
echo 'Enabled Desktop eviromentes to install:
0 - No desktop
1 - LXDE
'
read -p "Select the number of the desktop enviroment you wish to install, or 0 to not install any: " desktop
case $desktop in
	0 )
		echo 'none selected desktop'
	;;
	1 )
		touch /etc/apt/preferences.d/lightdm
		chmod 777 /etc/apt/preferences.d/lightdm
echo "Package: *lightdm*:any
Pin: origin *
Pin-Priority: -1" > /etc/apt/preferences.d/lightdm
	apt update
	apt install -y adwaita-icon-theme connman connman-gtk galculator gnome-themes-extra gparted leafpad lpkg lxappearance lxappearance-obconf lxhotkey-plugin-openbox lxinput lxde-core lxdm lxdm-loc-os mintstick obsidian-icon-theme picom plank playerctl rofi synaptic xorg yad
	touch /opt/Loc-OS-LPKG/installed-lpkg/LinksLpkgRepo.list
	wget https://gitlab.com/loc-os_linux/debian12-to-loc-os23/-/raw/main/lxde-config.tar.xz
	tar xvf lxde-config.tar.xz
	rm -f lxde-config.tar.xz
	cp -r lxde-config/usr/* /usr
	cp -r lxde-config/skel/.* /etc/skel/
	cp -r lxde-config/opt/Loc-OScc /opt
	cp -r lxde-config/opt/.CcLoc-OS /opt
	rm -rf lxde-config
	cp -rf /etc/skel/.* /home/*/
	chmod -R 777 /home/*/.*
	mv /usr/libexec/xscreensaver/xscreensaver-systemd /usr/libexec/xscreensaver/xscreensaver-sysvinit
	rm -f debian12-to-loc-os23.sh
	;;
	* )
		echo 'Invalid option, continuing without desktop'
	;;
esac

echo 'Finished, you can reboot now.'
rm -f lxde-finish.sh